Name:   openfriday
Version:  #VERSION#
Release:  #REVISION#%{?dist}
Summary:  So simple

Group:    Application/Internet
License:  GPL
URL:    http://thebuilder.com
Source: openfriday-%{version}.tar.gz
BuildRoot:  %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

#BuildRequires:
#Requires:

%description
So simple

%prep
%setup -q


%build


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/abd
cp $RPM_BUILD_DIR/%{name}-%{version}/thebuilder %{buildroot}/abd/thebuilder


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
/abd/thebuilder


%changelog
